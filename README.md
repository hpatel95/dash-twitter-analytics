# Dash_Tw_Analytics - Dash Project : Haris PATEL & Riyâz PATEL / M2 DSS - Dr.Zitouni 


## Overview 

This git contains the files needed for the Dash project: Twitter Scrapping & Analysis.
The dashboard, coded in Python with Dash, allows to scrape and analyse tweets related to one (or several) specified keyword(s). 
The script requires python version 3.9 or later to run.

**The dashboard is also available from: https://dash-ilis22.herokuapp.com/**

Notice: Via the website, requests that are too long (more than 30 seconds) are automatically stopped. 
The problem does not come from the dashboard, but from the cloud platform-as-a-service Heroku. Fixes will be carried out to overcome the problem with hosting.

The tool is applicable in several fields, including health, and allows :
- Scrapping of tweets for specific dates
- Scrapping tweets for a specific language
- Scrapping of tweets for one or more keywords (see "Advanced search" section)
- navigation in a table containing : Tweet content, date and time, number of re-tweets, number of replies, number of likes
- export to csv format of the search table
![Base de données](img/screen_df.png)

Two main analyses are performed from the results:

- time analysis : 
1. Number of tweets mentioning the search query per day
2. Posting pattern related to the search query (over the whole database, calculation of the number of tweets for each hour of a day)

![Analyse temporelle](img/screen_temporel.png)

- textual analysis :
1. Frequency of words related to the tweets (common words called 'stopwords' are excluded from the count)
2. Word cloud, relative to the tweets (common stopwords are excluded from the visualization

![Analyse textuelle](img/screen_textuel.png)


## Advanced Search

The tweet retrieval method is the same as Twitter's, so it is possible to perform advanced searches.

It is possible to retrieve only the tweets of a user, without specifying a keyword with the query "from:*twitter_username*".

Here are the main search possibilities.

![Recherche avancée](img/recherche_avancee.jpg)


The full range of search options can be found here: https://github.com/igorbrigadir/twitter-advanced-search


## Reference : 
https://github.com/JustAnotherArchivist/snscrape


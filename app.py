######## Imports
#### Imports
import dash
from dash import dcc
from dash import html
import dash_bootstrap_components as dbc
from dash import dash_table as dt
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate
from datetime import date
import plotly.express as px
import pandas as pd
import snscrape.modules.twitter as twitterScraper
from io import BytesIO
from wordcloud import WordCloud
import base64
import time
import nltk
from nltk.corpus import stopwords
import re

#### Imports

#### Stopwords - WC / Word Frequency
nltk.download('stopwords')

# Pattern to remove unwanted characters (emoticons, emoji, pictograms...) in the text of tweets
emoji_pattern = re.compile("["
                           u"\U0001F600-\U0001F64F"  
                           u"\U0001F300-\U0001F5FF"  
                           u"\U0001F680-\U0001F6FF"  
                           u"\U0001F1E0-\U0001F1FF"  
                           u"\U00002500-\U00002BEF"  
                           u"\U00002702-\U000027B0"
                           u"\U00002702-\U000027B0"
                           u"\U000024C2-\U0001F251"
                           u"\U0001f926-\U0001f937"
                           u"\U00010000-\U0010ffff"
                           u"\u2640-\u2642"
                           u"\u2600-\u2B55"
                           u"\u200d"
                           u"\u23cf"
                           u"\u23e9"
                           u"\u231a"
                           u"\ufe0f"  
                           u"\u3030"
                           "]+", flags=re.UNICODE)

# Pattern to remove urls from the text of tweets
url_pattern = re.compile(r'(https?://)?(www\.)?(\w+\.)?(\w+)(\.\w+)(/.+)?')


new_stopwords = stopwords.words('english') + stopwords.words('french') + stopwords.words('arabic') + stopwords.words(
    'german') + stopwords.words('italian') + stopwords.words('spanish')
stopwords_perso = ["c'est", "cest", "quil", "co", "qd", "alors", "si", "tant", "qua", "cela", "tout", "dun", "va",
                   "cette", "cet", "là", "leur", "donc", "ça"]
new_stopwords.extend(stopwords_perso)
#### Stopwords - WC / Word Frequency

#### Value of languages the dropdownlist for language selection
lang_options = [{'label': 'Arabic | العربية', 'value': 'ar'},
                {'label': 'British English | British English', 'value': 'en-gb'},
                {'label': 'English | English', 'value': 'en'}, {'label': 'French | français', 'value': 'fr'},
                {'label': 'German | Deutsch', 'value': 'de'}, {'label': 'Italian | italiano', 'value': 'it'},
                {'label': 'Spanish | español', 'value': 'es'}]
#### Value of languages the dropdownlist for language selection

######## Imports

######## Dash App

app = dash.Dash(__name__, external_stylesheets=[dbc.themes.MORPH])
app.title = 'Twitter Analytics Dashboard'
server = app.server

#### Layout
app.layout = dbc.Container([
    dbc.Row(dbc.Col(html.H1("Twitter Analytics Dashboard",
                            style={"textAlign": "center"}), width=22)),
    dbc.Row(dbc.Col(html.H1("  No-code Twitter scraping and analytics",
                            style={"textAlign": "center", "fontSize": 20}), width=22)),
    html.Hr(),

    html.Div(className='row1',
             style={'display': 'flex'},
             children=[
                 html.Div([
                         html.H1('Query', style={'fontSize': 20}),
                     dcc.Input(id='searchId', placeholder='Ex: vaccine, fever, covid', type='text'),
                     html.Hr(),
                     html.H1('Langue', style={'fontSize': 20}),
                     dcc.Dropdown(id='twitter_search_lang', placeholder='Language',
                                  options=lang_options, value='fr'
                                  )], style={'width': '33%', 'display': 'inline-block'}),


                 html.Div([
                     html.H2('Time range', style={'fontSize': 20}),
                     dcc.DatePickerSingle(
                         id='date1_Id',
                         min_date_allowed=date(2007, 8, 5),
                         date=date(2022, 1, 1),
                         display_format='YYYY-MM-DD'
                     ),
                     dcc.DatePickerSingle(
                         id='date2_Id',
                         min_date_allowed=date(2007, 8, 5),
                         date=date.today(),
                         display_format='YYYY-MM-DD'
                     )], style={'width': '33%', 'display': 'inline-block'}),

                 html.Div([
                     html.H3('Number of tweets', style={'fontSize': 20}),
                     dcc.Input(id='countId',
                               placeholder='Ex: 100, 200', type='number', value = 50),
                     dcc.Store(id='memory'),
                     html.Hr(),
                     html.Hr(),
                     dbc.Button("Launch", id="button", n_clicks=0, outline=True, color="primary", className="me-1"),

                     dbc.Spinner(html.Div(id="loading-output"), color="primary")],
                     style={'width': '33%', 'display': 'inline-block'})
             ]),

    html.Hr(),
    html.Div([
        dcc.Tabs(
            [
                dcc.Tab(label='Data Frame', children=[
                    dt.DataTable(
                        columns=[{'id': c, 'name': c} for c in
                                 ['Text', 'Datetime', 'RetweetCount', 'ReplyCount', 'LikeCount']],
                        id='tweet_table',
                        style_cell_conditional=[
                            {
                                'if': {'column_id': c},
                                'textAlign': 'left'
                            } for c in ['Date', 'Region']
                        ],
                        style_data={
                            'color': 'black',
                            'backgroundColor': 'white', 'whiteSpace': 'normal', 'height': 'auto'
                        },
                        style_data_conditional=[
                            {
                                'if': {'row_index': 'odd'},
                                'backgroundColor': 'rgb(220, 220, 220)',
                            }
                        ],
                        style_header={
                            'backgroundColor': 'rgb(210, 210, 210)',
                            'color': 'black',
                            'fontWeight': 'bold'
                        },
                        style_table={'overflowY': 'scroll', 'overflowX': 'scroll', 'height': 500}),

                    html.Hr(),
                    dbc.Button("Download CSV", id="btn_csv", size="sm", className="me-1", outline=True,
                               color="success"),
                    dcc.Download(id="download-dataframe-csv")
                ]),
                dcc.Tab(label='Time analysis', children=[
                    dcc.Graph(id='graph-day'),
                    dcc.Graph(id='graph-hour')
                ]),
                dcc.Tab(label='Textual analysis', children=[
                    dcc.Graph(id='tweet_word_count'),
                    html.Hr(),
                    dcc.RangeSlider(

                        id='range_frequency_number',
                        min=1,
                        max=200,
                        step=1,
                        value=[1, 200],
                        marks={
                            1: {'label': 'Minimum'},
                            50: {'label': '50'},
                            100: {'label': '100 most frequent words'},
                            200: {'label': '200'}
                        },
                        pushable=1,
                        tooltip={"placement": "bottom", "always_visible": False},
                        allowCross=False
                    ),
                    html.Hr(),
                    html.Img(id='image_wc', style={
                        'height': '100%',
                        'width': '100%'
                    })
                ])
            ])

    ]),
    html.P([html.A("Source code", href="https://gitlab.univ-lille.fr/haris.patel.etu/dash_tw_analytics"),

            ".  DASH Project : Haris PATEL - Riyâz PATEL / MSc Healthcare Data Science - Dr. Zitouni "
            ], style={"textAlign": "center"})
])


#### Layout


#### Creating and storing the scraped tweets dataframe
@app.callback(
    [Output("loading-output", "children"),
     Output('memory', 'data')],
    [Input("button", "n_clicks")],
    [State('searchId', 'value'),
     State('twitter_search_lang', 'value'),
     State('date1_Id', 'date'),
     State('date2_Id', 'date'),
     State('countId', 'value')]

)
def update_Frame(n_clicks, searchId, twitter_search_lang, date_since, date_until, count):
    if searchId is None:
        raise PreventUpdate

    if n_clicks:
        time.sleep(1)
        tweet_list = []
        for i, tweet in enumerate(twitterScraper.TwitterSearchScraper(
                query=searchId + " since:" + date_since + " until:" + date_until + " lang:" + twitter_search_lang).get_items()):
            if i > count:
                break
            tweet_list.append(
                [tweet.content, tweet.date, tweet.replyCount, tweet.retweetCount, tweet.likeCount
                 ])

        df = pd.DataFrame(tweet_list,
                          columns=['Text', 'Datetime', 'RetweetCount', 'ReplyCount', 'LikeCount'
                                   ])
        return f"Data loaded", df.to_dict(
            orient='records')


#### Creating and storing the scraped tweets dataframe


#### Displaying the dataframe
@app.callback(
    [
        Output(component_id='tweet_table', component_property='data'),

        Output(component_id='tweet_table', component_property='columns')],
    [Input(component_id='memory', component_property='data')]
)
def display_tweets(df):
    tweets = pd.DataFrame(
        df)
    columns = [{'name': col, 'id': col} for col in tweets.columns]
    data = tweets.to_dict(orient='records')
    return data, columns


#### Displaying the dataframe


#### Graph : Number of tweets per day
@app.callback(
    Output(component_id='graph-day', component_property='figure'),
    [Input('memory', 'data')],
    [State('date1_Id', 'date'),
     State('date2_Id', 'date')]
)
def update_div1(df, min_date_range, max_date_range):
    if df is None:
        raise PreventUpdate

    data = pd.DataFrame(
        df)

    data['date'] = pd.DatetimeIndex(data['Datetime']).date
    data['count'] = 1
    data_filtered = data[['date', 'count']]

    df_tweets_date = data_filtered.groupby(["date"]).sum().reset_index()

    df_tweets_date.set_index('date',
                             inplace=True)

    df_tweets_date = df_tweets_date.reindex(pd.date_range(min_date_range, max_date_range),
                                            fill_value=0)

    fig = px.line(df_tweets_date, x=df_tweets_date.index, y='count',
                  title='Daily number of tweets')

    fig.update_layout(xaxis=dict(title='Date'),
                      yaxis=dict(title='Tweet Count'), title_x=.5,
                      margin=dict(r=0))

    return fig


#### Number of tweets per day


#### Graph: Distribution of tweets over 24 hours
@app.callback(
    Output(component_id='graph-hour', component_property='figure'),
    [Input('memory', 'data')]
)
def update_div2(df):
    if df is None:
        raise PreventUpdate

    data = pd.DataFrame(df)
    data['hour'] = pd.DatetimeIndex(data['Datetime']).hour

    data['count'] = 1

    data = data[['hour', 'count']]

    data['hour'] = pd.to_datetime(data['hour'], format='%H')

    df_tweets_hourly = data.groupby(
        ["hour"]).sum().reset_index()

    df_tweets_hourly.set_index('hour', inplace=True)

    df_tweets_hourly = df_tweets_hourly.reindex(
        pd.date_range(start="1900-01-01 00:00:00", end="1900-01-01 23:00:00", freq="60min"),
        fill_value=0)

    df_tweets_hourly[
        'datetime'] = df_tweets_hourly.index
    df_tweets_hourly['hour'] = pd.DatetimeIndex(
        df_tweets_hourly['datetime']).hour

    fig = px.bar(df_tweets_hourly, x='hour', y='count', range_x=[0, 23],
                 title='Tweet distribution over 24 hours')

    fig.update_layout(xaxis=dict(title='Hour of the day'),
                      yaxis=dict(title='Tweet Count'), title_x=.5,
                      margin=dict(r=0))

    return fig


#### Graph: Distribution of tweets over 24 hours

#### Download button to download the dataframe of scrapped tweets, memory, in csv
@app.callback(
    Output('download-dataframe-csv', 'data'),
    [Input('btn_csv', 'n_clicks')],
    [State('memory', 'data')],
    prevent_initial_call=True,
)
def func(n_clicks, df):
    data = pd.DataFrame(df)
    return dcc.send_data_frame(data.to_csv, "tweets_scrapes.csv")


#### Download button to download the dataframe of scrapped tweets, memory, in csv

#### Graph : Tweet Word frequency and WordCloud

@app.callback(
    [Output(component_id='tweet_word_count', component_property='figure'),
     Output('image_wc', 'src')],
    [Input('memory', 'data'),
     Input('range_frequency_number', 'value')
     ]
)
def word_count_graph(df, count_value):
    if df is None:
        raise PreventUpdate

    data = pd.DataFrame(df)


    data['clean_text'] = data['Text'].apply(
        lambda x: ' '.join([word for word in x.lower().split() if word not in (new_stopwords)]))
    data['clean_text'] = data['clean_text'].str.replace(emoji_pattern, '', regex=True)
    data['clean_text'] = data['clean_text'].str.replace(url_pattern, '', regex=True)
    data['clean_text'] = data['clean_text'].str.replace(r'[^\w\s]+', ' ', regex=True)
    data['clean_text'] = data['clean_text'].apply(
        lambda x: ' '.join([word for word in x.split() if word not in (new_stopwords)]))

    countFreq = data.clean_text.str.split(expand=True).stack().value_counts()[:100]

    newGraph = countFreq[count_value[0]:count_value[1]]


    fig = px.bar(x=newGraph.index, y=newGraph.values, title="Tweet Word Frequency")

    fig.update_layout(xaxis=dict(title='Words'),
                      yaxis=dict(title='Count'), title_x=.5,
                      margin=dict(r=0))


    wc = WordCloud(background_color='#F0FFFE', width=800, height=600).generate_from_frequencies(newGraph)
    wc_img = wc.to_image()
    img = BytesIO()
    wc_img.save(img, format='PNG')

    return fig, "data:image/png;base64,{}".format(
        base64.b64encode(img.getvalue()).decode())


#### Graph : Tweet Word frequency and WordCloud

########  Dash App


if __name__ == '__main__':
    app.run_server(debug=True)
